package GUI;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;

import AStar.Outerloop;

public class Mainframe {

	private JFrame frame;
	private JPanel panel;
	private boolean mazeFlag;
	private static Mainframe window;
	private JLabel lblPathLength;
	private JLabel lblExtendedCells;
	private JLabel lblSearchCount;
	
	public static final int width=101;
	public static final int height=101;
	public static final int squareSize=7;
	public static final int borderHeight=44;
	public static final int blockProb=3;
	
	public int startPointRowIndex;
	public int startPointColumnIndex;
	public int targetRowIndex;
	public int targetColumnIndex;
	public int mode;
	public int switchOfAdptive;
	public int tie_mode;
	
	public Outerloop otl;
	
	public int[][][] maze=new int[height][width][7]; // [0]: environment(0:non-blocked 1:blocked 2:current point 3:target 4:path)
													 // [1]: agent memory(0:non-blocked 1:blocked)
													 // [2]: g value
													 // [3]: h value
													 // [4]: search value
													 // [5]: tree-point row index
													 // [6]: tree-point column index
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		//EventQueue.invokeLater(new Runnable() {
			//public void run() {
				//try {
		window = new Mainframe();
		window.frame.setVisible(true);
		//window.panel.repaint();
		//window.panel.repaint();
				//} catch (Exception e) {
					//e.printStackTrace();
				//}
			//}
		//});
	}

	/**
	 * Create the application.
	 */
	public Mainframe() {
		this.mode = 0;
		this.switchOfAdptive = 0;
		this.tie_mode=0;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		mazeFlag=false;
		frame = new JFrame();
		frame.setBounds(100, 100, Mainframe.squareSize*Mainframe.width, Mainframe.squareSize*Mainframe.height+Mainframe.borderHeight);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		
		panel = new JPanel(){
			//int count1=0,count2=0,count3=0;
			public void paint(Graphics graphics) {
				super.paint(graphics);
	            Graphics g2d = (Graphics2D) graphics;
	            if(!mazeFlag){
		            //g2d.fillRect(1*GuiTest.squareSize, 0*GuiTest.squareSize, GuiTest.squareSize, GuiTest.squareSize);
		            mazeGeneration(0,Mainframe.height-1,0,Mainframe.width-1,g2d);
		            
		            //pick up start and target point
		            while(true){
		            	startPointRowIndex = (int) Math.round(Math.random()*(Mainframe.height-1));
		            	startPointColumnIndex = (int) Math.round(Math.random()*(Mainframe.width-1));
		            	if(maze[startPointRowIndex][startPointColumnIndex][0]==0){
		            		maze[startPointRowIndex][startPointColumnIndex][0]=2;//start point
		            		g2d.setColor(Color.GREEN);
        					g2d.fillRect(startPointColumnIndex*Mainframe.squareSize, startPointRowIndex*Mainframe.squareSize, Mainframe.squareSize, Mainframe.squareSize);
		            		break;
		            	}
		            }
		            while(true){
		            	targetRowIndex = (int) Math.round(Math.random()*(Mainframe.height-1));
		            	targetColumnIndex = (int) Math.round(Math.random()*(Mainframe.width-1));
		            	if(maze[targetRowIndex][targetColumnIndex][0]==0){
		            		maze[targetRowIndex][targetColumnIndex][0]=3;//target point
		            		g2d.setColor(Color.RED);
        					g2d.fillRect(targetColumnIndex*Mainframe.squareSize, targetRowIndex*Mainframe.squareSize, Mainframe.squareSize, Mainframe.squareSize);
		            		break;
		            	}
		            }
		            for(int i=0;i<Mainframe.height;i++){
	            		for(int j=0;j<Mainframe.width;j++){
	            			maze[i][j][1]=0;
	            			maze[i][j][2]=0;
	            			maze[i][j][3]=Math.abs(i-targetRowIndex)+Math.abs(j-targetColumnIndex);
	            			maze[i][j][4]=0;
	            			maze[i][j][5]=0;
	            			maze[i][j][6]=0;
	            		}
	            	}
		            mazeFlag=true;
		            /*for(int i=0;i<height;i++){
		            	for(int j=0;j<width;j++){
		            		System.out.println("["+i+","+j+"]:"+maze[i][j]);
		            	}
		            }*/
		            /*g2d.setColor(Color.BLUE);
		            for(int i=0;i<height;i++){
		            	for(int j=0;j<width;j++){
		            		if(maze[i][j]){
		            			count3++;
		            			g2d.fillRect(j*GuiTest.squareSize, i*GuiTest.squareSize, GuiTest.squareSize, GuiTest.squareSize);
		            		}
		            	}
		            }
		            System.out.println(count1);
		            System.out.println(count2);
		            System.out.println(count3);*/
	            }else{
	            	for(int i=0;i<Mainframe.height;i++){
	            		for(int j=0;j<Mainframe.width;j++){
	            			switch (maze[i][j][0]){
		            			case 0: //non-block
	            					g2d.setColor(Color.WHITE);
	            					break;
	            				case 1: //block
	            					g2d.setColor(Color.GRAY);
	            					break;
	            				case 2: //start point
	            					g2d.setColor(Color.GREEN);
	            					break;
	            				case 3: //target
	            					g2d.setColor(Color.RED);
	            					break;
	            				case 4: //Path
	            					g2d.setColor(Color.PINK);
	            					break;
	            			}
	            			g2d.fillRect(j*Mainframe.squareSize, i*Mainframe.squareSize, Mainframe.squareSize, Mainframe.squareSize);
	            		}
	            	}
	            }
	            g2d.setColor(Color.GRAY);
	            for(int i=0;i<Mainframe.width;i++){
	            	for(int j=0;j<Mainframe.height;j++){
	            		g2d.drawRect(j*Mainframe.squareSize, i*Mainframe.squareSize, Mainframe.squareSize, Mainframe.squareSize);
	            	}
	            }
	            if(otl != null)
		             synchronized(otl){
		            		otl.notifyAll();
		             }
		 	}
		   
			private void mazeGeneration(int heightMin, int heightMax, int widthMin, int widthMax, Graphics g){
				int row = (int) Math.round(Math.random()*(heightMax-heightMin)+heightMin);
				int column = (int) Math.round(Math.random()*(widthMax-widthMin)+widthMin);
				
				for(int i=widthMin;i<=widthMax;i++){
					int blockOrNot = (int) Math.round(Math.random()*(10-1)+1);
					//g.fillRect(i*GuiTest.squareSize, row*GuiTest.squareSize, GuiTest.squareSize, GuiTest.squareSize);
					if(blockOrNot<=Mainframe.blockProb){
						//count1++;
						maze[row][i][0]=1; //block
						g.setColor(Color.GRAY);
					}else{
						//count2++;
						maze[row][i][0]=0; //non-block
						g.setColor(Color.WHITE);
					}
					g.fillRect(i*Mainframe.squareSize, row*Mainframe.squareSize, Mainframe.squareSize, Mainframe.squareSize);
				}
				
				for(int i=heightMin;i<=heightMax;i++){
					if(i!=row){
						int blockOrNot = (int) Math.round(Math.random()*(10-1)+1);
						//g.fillRect(column*GuiTest.squareSize, i*GuiTest.squareSize, GuiTest.squareSize, GuiTest.squareSize);
						if(blockOrNot<=Mainframe.blockProb){
							//count1++;
							maze[i][column][0]=1; //block
							g.setColor(Color.GRAY);
						}else{
							//count2++;
							maze[i][column][0]=0; //non-block
							g.setColor(Color.WHITE);
						}
						g.fillRect(column*Mainframe.squareSize, i*Mainframe.squareSize, Mainframe.squareSize, Mainframe.squareSize);
					}
				}
				
				if(heightMin<=row-1 && widthMin<=column-1)
					mazeGeneration(heightMin,row-1,widthMin,column-1,g);
				if(heightMin<=row-1 && column+1<=widthMax)
					mazeGeneration(heightMin,row-1,column+1,widthMax,g);
				if(row+1<=heightMax && widthMin<=column-1)
					mazeGeneration(row+1,heightMax,widthMin,column-1,g);
				if(row+1<=heightMax && column+1<=widthMax)
					mazeGeneration(row+1,heightMax,column+1,widthMax,g);
			}
			
			public void update(Graphics graphics) {
				paint(graphics);
			}
		};
		
		frame.getContentPane().add(panel);
		JMenuBar menuBar_3 = new JMenuBar();
		frame.getContentPane().add(menuBar_3, BorderLayout.NORTH);
		
		//Config Menu
		JMenu mnNewMenu = new JMenu("Config");
		menuBar_3.add(mnNewMenu);
		JRadioButtonMenuItem rdbtnmntmNewRadioItem = new JRadioButtonMenuItem("Forward");
		rdbtnmntmNewRadioItem.setSelected(true);
		mnNewMenu.add(rdbtnmntmNewRadioItem);
		JRadioButtonMenuItem rdbtnmntmNewRadioItem_1 = new JRadioButtonMenuItem("Backward");
		mnNewMenu.add(rdbtnmntmNewRadioItem_1);
		JRadioButtonMenuItem rdbtnmntmNewRadioItem_2 = new JRadioButtonMenuItem("Adaptive");
		mnNewMenu.add(rdbtnmntmNewRadioItem_2);
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(rdbtnmntmNewRadioItem);
		bg.add(rdbtnmntmNewRadioItem_1);
		bg.add(rdbtnmntmNewRadioItem_2);
		
		//Forward
		rdbtnmntmNewRadioItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mode=0;
				switchOfAdptive=0;
			}
		});
		
		//Backward
		rdbtnmntmNewRadioItem_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mode=1;
				switchOfAdptive=0;
			}
		});
		
		//Adaptive
		rdbtnmntmNewRadioItem_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mode=0;
				switchOfAdptive = 1;
			}
		});
		
		//Tie Breaking
		JMenu mnNewMenu_2 = new JMenu("Tie Breaking");
		menuBar_3.add(mnNewMenu_2);
		JRadioButtonMenuItem TBNewRadioItem = new JRadioButtonMenuItem("BIGGER G");
		TBNewRadioItem.setSelected(true);
		mnNewMenu_2.add(TBNewRadioItem);
		JRadioButtonMenuItem TBNewRadioItem_1 = new JRadioButtonMenuItem("SMALLER G");
		mnNewMenu_2.add(TBNewRadioItem_1);
		
		ButtonGroup bg1 = new ButtonGroup();
		bg1.add(TBNewRadioItem);
		bg1.add(TBNewRadioItem_1);
		
		//BIGGER_G
		TBNewRadioItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tie_mode=0;
			}
		});
		
		//SMALLER_G
		TBNewRadioItem_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tie_mode=1;
			}
		});		
		
		JMenu mnNewMenu_1 = new JMenu("Action");
		menuBar_3.add(mnNewMenu_1);
		//Reset Maze
		JMenuItem mntmNewMenuItem = new JMenuItem("Reset Maze");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mazeFlag=false;
				labelInitialize();
				panel.repaint();
			}
		});
		
		mnNewMenu_1.add(mntmNewMenuItem);
		
		//Start
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Start");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mnNewMenu.setEnabled(false);
				mnNewMenu_1.setEnabled(false);
				labelInitialize();
				new Thread(){
					public void run() {
						otl=new Outerloop(Mainframe.window);
						otl.MainLogic();
					}
				}.start();
					
				mnNewMenu.setEnabled(true);
				mnNewMenu_1.setEnabled(true);
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_1);
		
		//Restart
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Restart");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mnNewMenu.setEnabled(false);
				mnNewMenu_1.setEnabled(false);
				labelInitialize();
				for(int i=0;i<Mainframe.height;i++){
            		for(int j=0;j<Mainframe.width;j++){
            			if(maze[i][j][0]!=1){
            				maze[i][j][0]=0;
            			}
            			maze[i][j][1]=0;
            			maze[i][j][2]=0;
            			maze[i][j][3]=Math.abs(i-targetRowIndex)+Math.abs(j-targetColumnIndex);
            			maze[i][j][4]=0;
            			maze[i][j][5]=0;
            			maze[i][j][6]=0;
            		}
            	}
				maze[startPointRowIndex][startPointColumnIndex][0]=2;
				maze[targetRowIndex][targetColumnIndex][0]=3;
				panel.repaint();
				new Thread(){
					public void run() {
						otl=new Outerloop(Mainframe.window);
						otl.MainLogic();
					}
				}.start();
				mnNewMenu.setEnabled(true);
				mnNewMenu_1.setEnabled(true);
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_2);
		
		menuBar_3.add(new JSeparator(JSeparator.VERTICAL));
		
		lblPathLength = new JLabel("Path Length:       ");
		menuBar_3.add(lblPathLength);
		lblExtendedCells = new JLabel("Extended Cells:         ");
		menuBar_3.add(lblExtendedCells);
		lblSearchCount = new JLabel("Search Count:         ");
		menuBar_3.add(lblSearchCount);
	}
	
	public void showMessage(String msg){
		JOptionPane.showMessageDialog(null, msg, "Message", JOptionPane.WARNING_MESSAGE); 
	}
	
	public void repaint(){
		panel.repaint();
	}
	
	public void setPathLength(String length){
		lblPathLength.setText("Path Length: "+length+"    ");
	}
	
	public void setExtendedCells(String extendCells){
		lblExtendedCells.setText("Extended Cells: "+extendCells+"    ");
	}
	
	public void setSearchCount(String searchCount){
		lblSearchCount.setText("Search Count: "+searchCount+"    ");
	}
	
	private void labelInitialize(){
		lblPathLength.setText("Path Length:       ");
		lblExtendedCells.setText("Extended Cells:         ");
		lblSearchCount.setText("Search Count:         ");
	}
}
