package AStar;

import GUI.Mainframe;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

import BinaryHeap.Binary_heap;

public class Outerloop {
	private Mainframe mfrm;
	private int currentRowIndex;
	private int currentColumnIndex;
	private int targetRowIndex;
	private int targetColumnIndex;
	private int[][][] maze;
	private int mode;
	private int switchOfAdptive;
	private int tie_mode;
	private int extendedCells;
	private int pathLength;
	
	public Outerloop(Mainframe mfrm){
		this.mfrm = mfrm;
		currentRowIndex = mfrm.startPointRowIndex;
		currentColumnIndex = mfrm.startPointColumnIndex;
		targetRowIndex = mfrm.targetRowIndex;
		targetColumnIndex = mfrm.targetColumnIndex;
		maze=mfrm.maze;
		mode=mfrm.mode;
		switchOfAdptive=mfrm.switchOfAdptive;
		tie_mode=mfrm.tie_mode;
		extendedCells=0;
		pathLength=0;
	}
	
	//cell{0:environment,1:agent_memory,2:g,3:h,4:search,5:tree_point_row,6:tree_point_column} 
    //for environment: 0:unexpanded; 1:blocked; 2:start; 3:target;
    //for agent_memory: 0:unblocked; 1:blocked
    //for tree_pointer:0:NULL for row:1:father is up cell;-1:father is down cell; for column -1:left; +1:right
	//func:computePath
    private int computePath(int count){
    	int MAXNUMBER=Integer.MAX_VALUE;
    	int Const=Mainframe.height*Mainframe.width;// !!!
    	int numItems = Mainframe.width * Mainframe.height+1;
        Binary_heap h = new Binary_heap( numItems );
        LinkedList<int[]> closedList=new LinkedList<int[]>();
        int Starti,Startj,Targeti,Targetj;
        if (mode==1){// backward
        	Starti=targetRowIndex;
        	Startj=targetColumnIndex;
            Targeti=currentRowIndex;
            Targetj=currentColumnIndex;
        }
        else{ //forward
        	Starti=currentRowIndex;
        	Startj=currentColumnIndex;
            Targeti=targetRowIndex;
            Targetj=targetColumnIndex;
        }
        
        //set g value
        maze[Starti][Startj][2]=0;
        maze[Targeti][Targetj][2]=Integer.MAX_VALUE;
        
        //insert State(start) in OpenList
        int start_g=0;//maze[Starti][Startj][2];
        int start_h;
        if(switchOfAdptive == 1)
        	start_h = maze[Starti][Startj][3];
        else
            start_h=Math.abs(Targeti-Starti)+Math.abs(Targetj-Startj);//can be adapted
        
        //set h value
        maze[Starti][Startj][3]=start_h;
        
        int start_f = tie_mode==1? Const*(start_g+start_h)-start_h : Const*(start_g+start_h)-start_g;//tie_mode:0:bigger_g 1:smaller_g
        int[] startnode={start_f,Starti,Startj};
        h.insert(startnode);
        
        int[] mincell;
        int[] minValueCell = h.findMinCell();
    	while(!h.isEmpty() && maze[Targeti][Targetj][2]>(maze[minValueCell[0]][minValueCell[1]][2]+maze[minValueCell[0]][minValueCell[1]][3])){
    		mincell=h.deleteMin();
    		extendedCells++;
    		if(switchOfAdptive == 1)
    			closedList.add(mincell.clone());//insert copy of expanded cell
    		int father_g=maze[mincell[1]][mincell[2]][2];
    		int child_pos;
    		int[][] succ={{mincell[1]+1,mincell[2]},{mincell[1]-1,mincell[2]},{mincell[1],mincell[2]-1},{mincell[1],mincell[2]+1}};
    		for(int x=0;x<4;x++){
    			int i=succ[x][0];
    			int j=succ[x][1];
    			
    			child_pos=h.Pos2Index(i, j);
    			if (i>=0 && i<Mainframe.height && j>=0 && j<Mainframe.width && maze[i][j][1]!=1){
        			if (maze[i][j][4]<count){
        				maze[i][j][2]=MAXNUMBER;
        				maze[i][j][4]=count;
        			}
        			if(maze[i][j][2]>father_g+1){// c(s,a)=1
        				int child_g=maze[i][j][2]=father_g+1;
        				int child_h;
        				if(switchOfAdptive == 1)
        					child_h = maze[i][j][3];
        				else
        					child_h=Math.abs(Targeti-i)+Math.abs(Targetj-j);//can be adapted
        				
        				maze[i][j][3]=child_h;
        				
        				int child_f = (tie_mode==1)?Const*(child_g+child_h)-child_h:Const*(child_g+child_h)-child_g;//tie_mode:0:bigger_g 1:smaller_g
        				maze[i][j][5]=mincell[1];//rowIndex of parent
        				maze[i][j][6]=mincell[2];//columnIndex of parent
        				if (h.OLpos.containsKey(child_pos)){
        					h.change_g(h.OLpos.get(child_pos),child_f);
        				}
        				else {
        					int[] node={child_f,i,j};
        					h.insert(node);
        				}
        			}
        		}
    		}
    		minValueCell = h.findMinCell();
    	}
    	
    	if(switchOfAdptive == 1){
    		Iterator<int[]> it = closedList.iterator();
    		int gSgoal = maze[Targeti][Targetj][2];
    		while(it.hasNext()){
    			int[] cell = it.next(); 
    			maze[cell[1]][cell[2]][3] = gSgoal-maze[cell[1]][cell[2]][2];//hnew(s) = g(Sgoal)-g(S)
    		}
    	}
    	
    	if (h.isEmpty())
    		return 0;
    	else 
    		return 1;
    }
	
	public void MainLogic(){
		int counter=0;
		while((currentRowIndex!=targetRowIndex) 
				|| currentColumnIndex!=targetColumnIndex){
			counter++;
			//maze[currentRowIndex][currentColumnIndex][2] = 0;
			maze[currentRowIndex][currentColumnIndex][4] = counter;
			//maze[targetRowIndex][targetColumnIndex][2] = Integer.MAX_VALUE;
			maze[targetRowIndex][targetColumnIndex][4] = counter;
			
			//lookAround
			lookAround(currentRowIndex, currentColumnIndex);
			
			//Compute Path
			int rtValue = computePath(counter);
			if(rtValue == 0){
				mfrm.showMessage("I cannot reach the target");
				break;
			}
			
			//Backward Searchr
			if (mode==1){
				//Track Path for backward
				while(currentRowIndex!=targetRowIndex || currentColumnIndex!=targetColumnIndex){
					lookAround(currentRowIndex, currentColumnIndex);
					int tmpRowIndex = maze[currentRowIndex][currentColumnIndex][5];
					int tmpColumnIndex = maze[currentRowIndex][currentColumnIndex][6];
					if(maze[tmpRowIndex][tmpColumnIndex][1]==1){
						break;
					}
					else{
						maze[currentRowIndex][currentColumnIndex][0]=4;
						maze[tmpRowIndex][tmpColumnIndex][0]=2;
						currentRowIndex = tmpRowIndex;
						currentColumnIndex = tmpColumnIndex;
						pathLength++;
						mfrm.repaint();
						try {
							synchronized(this){
								this.wait();
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
			}
			//Forward Search
			else{
				//Track Path
				Stack<int[]> path = new Stack<int[]>();
				int[] ChildNode = {targetRowIndex, targetColumnIndex};
				while(ChildNode[0]!=currentRowIndex || ChildNode[1]!=currentColumnIndex){
					path.push(new int[]{ChildNode[0], ChildNode[1]});
					int tmpRowIndex = maze[ChildNode[0]][ChildNode[1]][5];
					int tmpColumnIndex = maze[ChildNode[0]][ChildNode[1]][6];
					ChildNode[0] = tmpRowIndex;
					ChildNode[1] = tmpColumnIndex;
				}
				
				
				//Walk
				while(!path.isEmpty()){
					int[] nextStep = path.pop();
					//lookAround
					lookAround(currentRowIndex, currentColumnIndex);
					if(maze[nextStep[0]][nextStep[1]][1]==1){
						break;
					}else{
						maze[currentRowIndex][currentColumnIndex][0]=4;
						maze[nextStep[0]][nextStep[1]][0]=2;
						currentRowIndex = nextStep[0];
						currentColumnIndex = nextStep[1];
						pathLength++;
						mfrm.repaint();
						try {
							synchronized(this){
								this.wait();
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}	
		}
		
		mfrm.setPathLength(String.valueOf(pathLength));
		mfrm.setExtendedCells(String.valueOf(extendedCells));
		mfrm.setSearchCount(String.valueOf(counter));
	}
	
	private void lookAround(int rowIndex, int columnIndex){
		//look around
		int[][] surround={{rowIndex-1,columnIndex},
					  	  {rowIndex+1,columnIndex},
					      {rowIndex,columnIndex-1},
					      {rowIndex,columnIndex+1}};
		for(int i=0;i<4;i++){
			int x=surround[i][0];
			int y=surround[i][1];
			if (x>=0 && x<Mainframe.height && y>=0 && y<Mainframe.width && maze[x][y][0]==1){
				maze[x][y][1]=1;
			}
		}
	}
}
