package BinaryHeap;


import java.util.ArrayList;
import java.util.HashMap;

import GUI.Mainframe;

public class Binary_heap {
	private static final int[] NULL = null;

	public Binary_heap(){
        this( DEFAULT_CAPACITY);
    }
	
	public Binary_heap( int capacity ){
        currentSize = 0;
        array = new int[ capacity + 1 ][];
    }
	
	public void insert( int[] x ) {
        int hole = ++currentSize;
        array[hole]=x;
        int pos=Pos2Index(x[1],x[2]);
        OLpos.put(pos, hole);
        percolateUp(hole); 
    }
	
	public int[] findMinCell(){
		if( isEmpty( ) )
            return null;
		return new int[] {array[1][1], array[1][2]};
	}
	
	public int findMin( ){// !! return the MinValue heap_pos
        if( isEmpty( ) )
            return 0;
        ArrayList<Integer> duplicate=new ArrayList<Integer>();
        duplicate.add(1);
        int minNUM=array[1][0];
        int now_index=0;
        int child;
        for(int heappos=1; heappos*2<=currentSize;heappos=duplicate.get(++now_index)){
        	child=heappos*2;
        	if (array[child][0]==minNUM)
        		duplicate.add(child);
        	if (child!=currentSize && array[child+1][0]==minNUM)
        		duplicate.add(child+1);
        	if (now_index==(duplicate.size()-1))
        		break;
        }
        //return 1;
        //System.out.println(duplicate.size());
        return  duplicate.get((int)Math.round(Math.random()*(duplicate.size()-1)));
    }
	
	public int[] deleteMin( ){
        if( isEmpty( ) )
            return NULL;
        int minItem_heappos = findMin( );
        //System.out.println(minItem_heappos);
        return deleteNUM(minItem_heappos);
    }
	
	public int[] deleteNUM(int heappos){
		if( isEmpty( ) )
             return NULL;
		int[] tmp=array[heappos];
		int pos_last=Pos2Index(array[currentSize][1],array[currentSize][2]);
        int pos_heappos=Pos2Index(array[heappos][1],array[heappos][2]);
		if (currentSize!=1){
			array[heappos]= array[ currentSize-- ];
			OLpos.remove(pos_heappos);
			OLpos.put(pos_last, heappos);
			if (!percolateDown( heappos )){
				percolateUp(heappos);
			}
		}
		else{
			currentSize--;
			OLpos.remove(pos_heappos);
		}
		
		return tmp;
	}
	
	public int change_g(int heappos,int new_g){
		if( isEmpty( ) )
             return 0;
		array[heappos][0]= new_g;
		if (!percolateDown( heappos )){
			percolateUp(heappos);
		}
		return 1;
	}
	
	private void buildHeap( ){
		for( int i = currentSize / 2; i > 0; i-- )
            percolateDown( i );
    }
	
	public void printHeap( ){
		for( int i = 1; i < currentSize+1; i++ ){
			System.out.printf("%d %d %d \n",array[i][0],array[i][1],array[i][2]);
		}
    }
	
	public boolean isEmpty( ){
        return currentSize == 0;
    }
	
	public boolean isFull( ){
        return currentSize == array.length - 1;
    }

    public void makeEmpty( ){
        currentSize = 0;
        OLpos.clear();
    }

    private static final int DEFAULT_CAPACITY = 12000;
    private int currentSize;      // Number of elements in heap
    private int[][] array; // The heap array

    /**
     * Internal method to percolate down in the heap.
     * @param hole the index at which the percolate begins.
     */
    private boolean percolateDown( int hole ){
    	if (hole==0||currentSize<2){
    		return false;
    	}
    	int child;
    	boolean flag=false;
    	int[] tmp = array[hole];
    	for( ; hole * 2 <= currentSize; hole = child ){
    		child = hole * 2;
    		if( child != currentSize && array[child+1][0] < array[child][0] )
    			child++;
    		if( array[ child ][0] < tmp[0]  ){
    			int pos_child=Pos2Index(array[child][1],array[child][2]);
    			array[ hole ] = array[ child ];
    			OLpos.put(pos_child, hole);
    			flag=true;
    		}
            else
            	break;
        }
    	int pos_this=Pos2Index(tmp[1],tmp[2]);
    	array[hole] = tmp;
    	OLpos.put(pos_this, hole);
    	return flag;
    }
    
    private Boolean percolateUp(int hole){
    	if (hole==0||currentSize<2){
    		return false;
    	}
		int[] tmp=array[hole];
		for( ; hole> 1 && tmp[0] < array[ hole / 2 ][0]  ; hole /= 2 ){
			int pos_father=Pos2Index(array[hole/ 2 ][1],array[hole/ 2 ][2]);
            array[ hole ] = array[ hole/ 2 ];
			OLpos.put(pos_father, hole);
		}
		int pos_this=Pos2Index(tmp[1],tmp[2]);
        array[ hole ] = tmp;
        OLpos.put(pos_this, hole);
        return true;
	}
    
    
    public int Pos2Index(int i,int j){
    	return i*Mainframe.height+j;
    }
    
    
    public HashMap<Integer,Integer> OLpos=new HashMap<Integer,Integer>();
    
    

    
    
    
    
  
   // Test binary_heap program
    public static void main( String [ ] args )
    {
        int numItems = 10202;
        Binary_heap h = new Binary_heap( numItems );
        int[][]a={{8,0,0},{6,0,1},{9,1,0},{8,1,1},{23,0,2},{26,0,3},{19,0,4},{8,1,2}};
        //
        for (int i=0;i<a.length;i++){
        	h.insert(a[i]);
        	//System.out.println(h.OLpos.size());
        }
        h.printHeap();
        //h.change_g(h.OLpos.get(h.Pos2Index(0, 1)), 8);
        //h.printHeap();
        if (!h.isEmpty() && 10>(h.deleteMin())[0]){
        	
        }
        h.printHeap();

    }
}
